---
title: "FLOSS Week Sponsorship - IME-USP"
lang: en
ref: flossweek
layout: default
---

{% include add_multilang.html %}

The Computer Science Department of the main campus of the University of São Paulo, a part of the
Mathematics and Statistics Institute -- home of the Computer Science, Math, Applied Math and
Statistics departments -- will host, on April 13-18, the "Free/Libre/Open Source Software (FLOSS)
week" event, which aims to boost FLOSS awareness and adoption among students of the Institute. We
plan on hosting 2 to 3 sessions per day, including talks, workshops and Q&As. Some of the planned
topics are:

- What is free software and the free software licenses
- The history of GNU/Linux at USP and Brazil
- Customizing your GNU/Linux
- LaTeX workshop
- Basic and advanced Git workshop
- Software debugging workshop
- FLOSS graphical tools workshops (Godot, Inkscape and Blender)
- Amongst others! (Contact us if you are interested in offering a 1 hour talk or 3 hours workshop)

We are also planning a HackDay to complete the week encouraging students to be a part of the FLOSS
world. As an introduction to this activity, we will talk about how to contribute to FLOSS projects
and, help interested students in their first contributions. (If you have suggestions on projects
and/or issues that might be suitable for newcomers, please, let us know).

This event is organized by FLUSP (a student group dedicated to contributing to high-profile FLOSS
projects such as gcc, git, and the Linux kernel) and partnered with other student groups. This
presents an excellent opportunity for companies that develop or widely use FLOSS to showcase their
brand and involvement to newcomers to the community likely to become highly-qualified
professionals.

With these goals in mind, and aiming to provide an excellent experience for participants, we are
looking for sponsors to support us in this event. Our main priority is to collect resources for
investing in coffee breaks, printing flyers/banners/badges, and production of souvenirs for
participants.  We offer the following packages (all prices in USD):

|                                                       | Supporter <br> <sub> < $140 </sub> | Bronze <br> <sub> $140 </sub>  | Silver <br> <sub> $230 </sub> | Gold <br> <sub> $460 </sub>     | Platinum <br> <sub> ≥ $690 </sub>   |
|-------------------------------------------------------|-----------|--------|---------|---------|---------------|
| Logo on sponsors section of the event page            | Small     | Small  | Medium  | Large   | Largest       |
| Logo on promotional material (banners, posters, etc.) |           | Small  | Medium  | Large   | Largest       |
| Logo sponsors page of FLUSP website                   |           | ✓      | ✓       | ✓       | ✓             |
| Logo on the T-shirt                                   |           |        | ✓       | ✓       | ✓             |
| Mention at opening/closing                            |           |        | ✓       | ✓       | ✓             |
| Opportunity to display banner at the event            |           |        |         | ✓       | ✓             |
| Exclusivity on sponsor plan                           |           |        |         |         | Yes <br> <sup><sup> (2 slots) </sup></sup> |
| Opportunity to make 15 minutes speech at opening      |           |        |         |         | ✓             |

Transparency is embedded in the roots of FLUSP values. For this reason, we emphasize that any
exceeding funding received by the organization of the event will be reverted for the following
purposes:

* Finance the visit of international speakers (contingent to the funds' availability).
* Paying for FLUSP swag (T-shirt, cups, etc.) for group members and guests.
* Fund future events.
* Support students attending FLOSS events.
* Help FLUSP in its mission and goals related to FLOSS promotion at USP.


Sponsoring this event is helping disseminate the FLOSS culture in Brazil and
investing in the formation of new free software developers in the country.
