var schedule = [
  {
    date: 'Segunda-feira 13/4',
    timeline: [
      {
        start: '14:00',
        end: '16:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Coffee Break'
      },
      {
        start: '17:00',
        end: '19:00',
        title: '',
        speaker: '',
      }
    ]
  },
  {
    date: 'Terça-feira 14/4',
    timeline: [
      {
        start: '10:00',
        end: '12:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Almoço',
      },
      {
        start: '14:00',
        end: '16:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Coffee Break',
      },
      {
        start: '17:00',
        end: '19:00',
        title: '',
        speaker: '',
      },
    ]
  },
  {
    date: 'Quarta-feira 15/4',
    timeline: [
      {
        start: '10:00',
        end: '12:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Almoço',
      },
      {
        start: '14:00',
        end: '16:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Coffee Break',
      },
      {
        start: '17:00',
        end: '19:00',
        title: '',
        speaker: '',
      },
    ]
  },
  {
    date: 'Quinta-feira 16/4',
    timeline: [
      {
        start: '10:00',
        end: '12:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Almoço',
      },
      {
        start: '14:00',
        end: '16:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Coffee Break',
      },
      {
        start: '17:00',
        end: '19:00',
        title: '',
        speaker: '',
      },
    ]
  },
  {
    date: 'Sexta-feira 17/4',
    timeline: [
      {
        start: '10:00',
        end: '12:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Almoço',
      },
      {
        start: '14:00',
        end: '16:00',
        title: '',
        speaker: '',
      },
      {
        title: 'Coffee Break',
      },
      {
        start: '17:00',
        end: '19:00',
        title: '',
        speaker: '',
      },
    ]
  },
]
