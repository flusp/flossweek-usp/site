---
title: "Patrocínio para Semana do Software Livre - IME-USP"
lang: pt
ref: flossweek
layout: default
---

{% include add_multilang.html %}

O Departamento de Ciência da Computação do campus principal da Universidade de São Paulo, que é
parte do Instituto de Matemática e Estatística -- abrigando os cursos de Ciência da Computação,
Matemática, Matemática Aplicada e Estatística -- organizará, entre os dias 13-18 de Abril, a
"Semana de Software Livre", focando em conscientizar e incentivar a adoção de software livre entre
os estudantes do Instituto. Planejamos realizar 2 a 3 sessões por dia, incluindo palestras,
workshops e Q&As. Alguns dos tópicos que planejamos abordar são:

- O que é software livre e o que são licenças de software livre
- A história da chegada do GNU/Linux na USP e no Brasil
- Como customizar sua instalação GNU/Linux
- Workshop de LaTeX
- Workshop de Git básico e avançado
- Workshop de Debugging de Software
- Workshop the ferramentas gráficas livres (Godot, Inkscape e Blender)
- E outros! (Consulte-nos se você tiver interesse em oferecer uma palestra de 1 hora ou workshop de 3 horas.)

Também estamos planejando encerrar a semana com um HackDay, para incentivar os alunos a fazer parte
do mundo FLOSS. Neste dia, falaremos sobre como contribuir com projetos FLOSS e ajudaremos
estudantes em suas primeiras contribuições. (Caso você tenha sujestões de projeto e/ou issues
interessantes para iniciantes, por favor nos avise.)

Este evento é organizado pelo FLUSP (um grupo de extensão dedicado a contribuir para projetos FLOSS
de alto perfil, como o gcc, git, e o Kernel Linux) em parceria com outros grupos de extensão. Isso
apresenta uma excelente oportunidade para as empresas que desenvolvem ou fortemente adotam FLOSS
para exibir suas marcas e envolvimento para os novatos na comunidade, que virão a ser profissionais
altamente qualificados.

Com esses objetivos em mente, e focando em oferecer uma experiência excelente para nossos
participantes, estamos buscando por patrocinadores neste evento.  Nossas principais prioridades são
coletar recursos para investir em comida, impressão de cartazes/banners/crachás, e a produção de
brindes para os participantes. Nós oferecemos os seguintes pacotes:


|                                                                | Supporter <br> <sub> < R$600 </sub> | Bronze <br> <sub> R$600 </sub>  | Silver <br> <sub> R$1000 </sub> | Gold <br> <sub> R$2000 </sub>     | Platinum <br> <sub> ≥ R$3000 </sub>   |
|----------------------------------------------------------------|-----------|----------|----------|------------|---------------|
| Logo na seção de patrocinadores do evento                      | Pequeno   | Pequeno  | Médio    | Grande     | Maior         |
| Logo em material promocional (banners, pôsteres, etc.)         |           | Pequeno  | Médio    | Grande     | Maior         |
| Logo na página de patrocinadores do FLUSP                      |           | ✓        | ✓        | ✓          | ✓             |
| Logo na camiseta                                               |           |          | ✓        | ✓          | ✓             |
| Menção na abertura/fechamento                                  |           |          | ✓        | ✓          | ✓             |
| Oportunidade de exibir um banner no evento                     |           |          |          | ✓          | ✓             |
| Exclusividade no plano de patrocinadores                       |           |          |          |            | Sim <br><sup><sup> (2 slots) |
| Oportunidade de apresentação de 15 minutos no início do evento |           |          |          |            | ✓             |

A transparência está na raiz dos valores do FLUSP. Por esse motivo, destacamos
que todo valor excedente da organização do evento será revertido para os
seguintes fins:

* Financiar a visita de palestrantes internacionais (em função do dinheiro disponível).
* Confecção de brindes (camisetas, copos, etc.) do FLUSP para os membros do grupo e convidados.
* Financiar eventos futuros.
* Patrocinar estudantes a comparecer em eventos FLOSS.
* Ajudar o FLUSP em sua missão e objetivos relacionados à promoção de sofware livre na USP.

Patrocinar este evento é ajudar a disseminar a cultura FLOSS no Brasil e
investir na formação de novos desenvolvedores de software livre no pais.
